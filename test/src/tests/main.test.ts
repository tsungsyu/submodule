// import { sum } from "../scripts";

// test("adds 1 + 2 to equal 3", () => {
//   expect(sum(1, 2)).toBe(3);
// });
import 'expect-puppeteer'

describe('My Client', () => {
  beforeAll(async () => {
    await page.goto('http://client:3000')
  })

  it('should display "Praxhub" text on page', async () => {
    await expect(page).toMatch('Praxhub')
  })
})