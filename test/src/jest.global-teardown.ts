// global-teardown.js
const { teardown: teardownPuppeteer } = require("jest-environment-puppeteer");

module.exports = async function globalTeardown(globalConfig: any) {
  console.log("Global tear down");
  // Your global teardown
  await teardownPuppeteer(globalConfig);
};
